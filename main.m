clear all;close all;clc;

%% Detect ORBFeatures

I = imread('img.jpg');
I = rgb2gray(I);
figure;imshow(I)

% Detect and store ORB keypoints.
points = detectORBFeatures(I);

% Display the grayscale image and plot the detected ORB keypoints.
figure
imshow(I)
hold on
plot(points,'ShowScale',false)
hold off


%% Detect FASTFeatures

% Find the corners.
corners = detectFASTFeatures(I);
figure;imshow(I); hold on
plot(corners);

% Display the results.
[features, valid_corners] = extractFeatures(I, corners);
figure; imshow(I); hold on
plot(valid_corners);


%% Match Features

% Read images and find the corners.
I1 = rgb2gray(imread('fig/origin_gray.jpg'));
I2 = rgb2gray(imread('fig/origin_rot2.png'));
points1 = detectORBFeatures(I1);
points2 = detectORBFeatures(I2);

% Extract the neighborhood features.
[features1,valid_points1] = extractFeatures(I1,points1);
[features2,valid_points2] = extractFeatures(I2,points2);

% Match the features.
indexPairs = matchFeatures(features1,features2);

% Retrieve the locations of the corresponding points for each image.
matchedPoints1 = valid_points1(indexPairs(:,1),:);
matchedPoints2 = valid_points2(indexPairs(:,2),:);

% Visualize the corresponding points. 
figure; showMatchedFeatures(I1,I2,matchedPoints1,matchedPoints2);
